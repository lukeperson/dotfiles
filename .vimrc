execute pathogen#infect()
syntax on
filetype plugin indent on 

"=============== Vim commands ==============
set visualbell
set nowrap 

"Line numbers
set number
set relativenumber
colorscheme fruidle

"Always show cursor
set ruler
set showcmd 

"Tabs to spaces 
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent

"Use mouse, show line
set mouse=a

"Autload files that have changed outside vim
set autoread

"=============== Plugin ==============
"NerdTree shortcut 
map <C-n> :NERDTreeToggle<CR> 
"Close vim if only window left open is NERDTree utocmd bufenter * if
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endifset ignorecase


let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

":AirlineTheme <theme> to test different themes 
let g:airline_theme='violet'

"=============== Key Binds ==============
set nocompatible 
"filetype plugin on TODO: I have this at top test later 

map ;s :setlocal spell! spelllang=en_au<CR> 
map ;z 1z=

autocmd FileType html inoremap ;i <em></em><Space><Esc>Fet>i
