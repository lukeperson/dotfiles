#!/usr/bin/bash 

sudo cp ~/.vimrc ~/dotfiles/
sudo cp ~/.bashrc ~/dotfiles/
sudo cp ~/.tmux.conf ~/dotfiles/
sudo cp -R ~/.config/i3 ~/dotfiles/ 
#sudo cp -R ~/.vim ~/dotfiles/ 
sudo cp -R ~/.fonts ~/dotfiles/ 

