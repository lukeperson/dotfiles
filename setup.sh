#!/usr/bin/bash 
sudo rm ~/.vimrc
sudo rm ~/.bashrc
sudo rm -rf ~/.conf.i3
#sudo rm -rf ~/.vim 
sudo rm -rf ~/.fonts 

sudo cp  ~/dotfiles/ ~/.vimrc
sudo cp ~/dotfiles/ ~/.bashrc 
sudo cp ~/dotfiles/ ~/.tmux.conf 
sudo cp -R ~/dotfiles/ ~/.config.i3 
#sudo cp -R ~/dotfiles/ ~/.vim  
sudo cp -R ~/dotfiles/ ~/.fonts  
